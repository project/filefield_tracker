<?php
/**
 * @file
 * Contains VBO action definition and code for zip archive with tracking.
 */

/**
 * Implements hook_action_info().
 */
function filefield_tracker_archive_action_info() {
  return array(
    'filefield_tracker_archive_action' => array(
      'type' => 'file',
      'description' => t('Download zip archive of selected files and track with Filefield Tracker.'),
      'configurable' => FALSE,
      'aggregate' => TRUE,
    ),
  );
}

/**
 * Action callback.
 */
function filefield_tracker_archive_action($fids, $context) {
  // Iterate on files.
  $files = array();
  $placeholders = db_placeholders($fids);
  $f = db_query("SELECT * FROM {files} WHERE fid IN ($placeholders)", $fids);
  while ($file = db_fetch_object($f)) {
    $files[] = $file;
  }
  filefield_tracker_archive_action_do($files, $context);
}

/**
 * Do callback from action.
 */
function filefield_tracker_archive_action_do($files, $context) {
  // Create zip file.
  $zipfile = tempnam(file_directory_temp(), 'zip');
  $zip = new ZipArchive();
  if (!$zip->open($zipfile, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)) {
    return;
  }

  // Determine the page.
  $path = explode('/', drupal_get_normal_path(implode('/', arg())));
  $nid = ($path[0] == 'node' && is_numeric($path[1])) ? $path[1] : 0;

  foreach ($files as $file) {
    // Track the download.
    filefield_tracker_track_download(array('fid' => $file->fid), $nid);
    $zip->addFile(file_create_path($file->filepath), $file->filename);
  }
  $zip->close();

  // Download zip file.
  $view = views_get_view($context['view']['vid']);
  $zipname = $view->name . '-' . date('Ymd-His') . '.zip';
  header('Content-Type: application/zip');
  header('Content-Disposition: attachment; filename=' . $zipname);
  header('Content-Transfer-Encoding: binary');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
  header('Expires: 0');
  header('Content-Length: ' . filesize($zipfile));
  readfile($zipfile);
  unlink($zipfile);
  exit;
}
